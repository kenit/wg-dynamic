package config

import (
	"bytes"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
	"os"
)

type Config struct {
	Interface baseInterface `yaml:"Interface" validate:"required"`
	Peer      []basePeer    `yaml:"Peer" validate:"dive"`
}

type baseInterface struct {
	// base part
	PrivateKey string `yaml:"PrivateKey" validate:"required,base64"`
	ListenPort int    `yaml:"ListenPort,omitempty" validate:"omitempty,gte=0,lte=65535"`
	FwMark     int    `yaml:"FwMark,omitempty" validate:"omitempty,gte=0,lte=4294967295"`
	// extended part
	Address  []string `yaml:"Address,omitempty" validate:"omitempty,dive,cidr"`
	PreUp    string   `yaml:"PreUp,omitempty"`
	PostUp   string   `yaml:"PostUp,omitempty"`
	PreDown  string   `yaml:"PreDown,omitempty"`
	PostDown string   `yaml:"PostDown,omitempty"`
}

type basePeer struct {
	// base part
	PublicKey           string   `yaml:"PublicKey" validate:"required,base64"`
	PresharedKey        string   `yaml:"PresharedKey,omitempty" validate:"omitempty,base64"`
	AllowedIPs          []string `yaml:"AllowedIPs,omitempty" validate:"omitempty,dive,cidr"`
	Endpoint            string   `yaml:"Endpoint,omitempty" validate:"omitempty,udp_addr|hostname_port"`
	PersistentKeepalive int      `yaml:"PersistentKeepalive,omitempty" validate:"omitempty,gte=0,lte=65535"`
	// extended part
	Address []string `yaml:"Address,omitempty" validate:"omitempty,dive,cidr"`
}

//ParseRawConfig get config from raw bytes
func ParseRawConfig(config []byte) (*Config, error) {
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(config))
	if err != nil {
		return nil, err
	}
	yaml := Config{}
	err = viper.Unmarshal(&yaml)
	if err != nil {
		return nil, err
	}
	return &yaml, nil
}

//ParseFileConfig get config from file
func ParseFileConfig(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	return ParseRawConfig(data)
}

//Validate check valid, if no error -> ok
func (c Config) Validate() error {
	valid := validator.New()
	return valid.Struct(c)
}
