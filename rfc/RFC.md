# WG-DYNAMIC
## NAME
wg-dynamic - set up a WireGuard interface simply
## SYNOPSIS
wg-dynamic [ up | down | save ] [ CONFIG_FILE | INTERFACE ]
##DESCRIPTION
TODO
### Инициализация
#### Клиент
При обнаружении в адресах `auto` генерирует и назначает себе адрес из диапазона `fe80::/64`.
Добавляет в AllowedIPs всех PeerToml `fe80::/64`.
Генерирует для каждого PeerToml временный адрес из диапазона локальных адресов и добавляет их в AllowedIPs и маршруты.
#### Сервер
Назначает себе локальный адрес `fe80::/128` в локальной зоне видимости link-local.
Добавляет в AllowedIPs всех PeerToml `fe80::/64` и начинает прослушивать `fe80::/64%wg`.
### Поиск сервера
#### Клиент
Создаёт сообщение определения сервера содержащие его PublicKey.
Выполняет массовую рассылку этого сообщения по сгенерированным на инициализации адресам.
При получении ответа идентифицирует сервер, прописывает в его AllowedIPs `fe80::/128`, удаляет временные адреса
и отправляет подтверждение на адрес `fe80::/128` что сервер признан и установлен.
#### Сервер
При получении сообщения определения сервера определяется IP отправителя и при помощи PublicKey идентифицируется PeerToml. 
Для идентифицированного пира добавляется маршрут и AllowedIP вида `fe80::abcd:abcd:abcd:abcd/128`.
На адрес отправителя отправляется сообщение что сервер найден, и ожидается подтверждение.
### Получение адреса в аренду
1. Клиент создаёт сообщение запроса адреса. В сообщении указывается желаемая сеть/адрес (необязательно) и оно отправляется на сервер.
2. Сервер получая запрос проверяет наличие у клиента статического адреса, если он есть то он отправляется в сообщении предложения, 
с указанием нулевой длительности что говорит о том что он статичный.
Если статичного адреса нет, то выбирается адрес из AddressPool, и отправляется предложение клиенту.
3. Клиент получив предложение проверяет полученный IP и либо принимает его, либо отказывается.

В случае отказа клиента:
1. Клиент шлёт отказ, и начинает запрос заново.
2. Сервер в случае получения отказа, или не получении ответа в течении 90 сек делает предложение недействительным и сам посылает отказ не требующий ответа.
    
В случае принятия предложения клиентом:
1. Клиент высылает уведомление о принятии предложения данного конкретного предложения.
2. Сервер получив уведомление о принятии прописывает данный адрес клиенту.
На адрес клиента отправляется сообщение о готовности сервера.
3. Клиент получив уведомление о готовности сервера прописывает себе данный адрес.
И с нового адреса отправляет healthcheck серверу.
4. Сервер получив healthcheck, отправляет healthcheck обратно на выданный клиенту адрес.
5. Клиент получив healthcheck от сервера отправляет успешное завершение сессии серверу, считает процесс оконченным.
6. Сервер получив подтверждение конфига считает процесс оконченным.
### Продление аренды
1. Клиент создаёт запрос адреса указывая продлеваемый адрес как желаемый (с пометкой что это продление).
2. Сервер проверяет возможность продления. В случае отказа высылает отказ. В случае принятия высылает принятие с новой длительностью.

Примечание: если нет ответа более 90 секунд, то сессия считается проваленной. Все изменения откатываются процесс начинается снова.
При получении запроса от недействительной сессии он игнорируется.
#### Сервер
При получении сообщения определения сервера определяется IP отправителя и при помощи PublicKey идентифицируется PeerToml. 
Для идентифицированного пира добавляется маршрут и AllowedIP вида `fe80::abcd:abcd:abcd:abcd/128`.
На адрес отправителя отправляется сообщение что сервер найден, и ожидается подтверждение.
Если нет ответа более 90 секунд, то сессия считается проваленной. Все изменения откатываются процесс начинается снова.
При получении запроса от недействительной сессии он игнорируется.

###Работа сервера
1. Сервер назначает себе локальный адрес `fe80::/128` в локальной зоне видимости `link-local` и добавляет в AllowedIPs всех PeerToml `fe80::/64` и начинает прослушивать `fe80::/64%wg0`
2. При получении сообщения с публичным ключом клиента он идентифицируется и IP адрес с которого пришло сообщение добавляется данному PeerToml в виде `fe80::abcd:abcd:abcd:abcd/128` и создаётся соответствующий маршрут
3. Клиенту отправляется подтверждение что сервер найден.
## CONFIGURATION
### SERVER CONFIGURATION
Конфигурационный файл основан на YAML. Есть две секции -- Interface и Peer.
* Interface может содержать следующие поля:
    * PrivateKey — base64 приватный ключ сгенерированный wg genkey. Обязательный.
    * Address — список IP (v4 или v6) адресов (опционасльно с CIDR масками) которые будут назначены интерфейсу. Необязталеьный.
    * ListenPort — 16-bit порт для прослушивания. Необязательный; если не назначен выбирается случайным образом.
    * FwMark — 32-bit fwmark для исходящих пакетов. Если установлено "off", то эта функция выключена.
      Если установлен "0", "auto" или вообще не задан, то назначается автоматически. Необязательный.
    * Table - указывает таблицу маршрутизации которая будет использоваться. Если установлено "off", то эта функция выключена.
      Если установлен "0", "auto" или вообще не задан, то назначается автоматически. Необязательный.
    * MTU - устанавливает значение MTU на интерфейсе, принцип аналогичен FwMark и Table. Необязательный.  
    * PreUp, PostUp, PreDown, PostDown - bash скрипты, которые будут выполнены перед/после включения/выключения интерфейса,
      где %i имя интерфейса. Необязательные.
* Peer секции могут содержать следующие поля:
    * Address — список IP адресов которые будут выданы пиру по запросу без срока давности. Необязталеьный.
    * PublicKey — a base64 public key calculated by wg pubkey from a private key, and usually transmitted out of band to the author of the configuration file. Required.
    * PresharedKey — a base64 preshared key generated by wg genpsk. Optional, and may be omitted. This option adds an additional layer of symmetric-key cryptography to be mixed into the already existing public-key cryptography, for post-quantum resistance.
    * AllowedIPs — a comma-separated list of IP (v4 or v6) addresses with CIDR masks from which incoming traffic for this Peers is allowed and to which outgoing traffic for this Peers is directed. The catch-all 0.0.0.0/0 may be specified for matching all IPv4 addresses, and ::/0 may be specified for matching all IPv6 addresses. May be specified multiple times.
    * Endpoint — an endpoint IP or hostname, followed by a colon, and then a port number. This endpoint will be updated automatically to the most recent source IP address and port of correctly authenticated packets from the Peers. Optional.
    * PersistentKeepalive — a seconds interval, between 1 and 65535 inclusive, of how often to send an authenticated empty packet to the Peers for the purpose of keeping a stateful firewall or NAT mapping valid persistently. For example, if the interface very rarely sends traffic, but it might at anytime receive traffic from a Peers, and it is behind NAT, the interface might benefit from having a persistent keepalive interval of 25 seconds. If set to 0 or "off", this option is disabled. By default or when unspecified, this option is off. Most users will not need this. Optional.
* DHCP секция может содержать следующие поля
    * AddressPool - список IP (v4 или v6) сетей адреса из которых будут выдаваться при запросе. Необязталеьный.
    * Rent - время в секундах на которое выдаётся IP адрес


### CLIENT CONFIGURATION
Конфигурационный файл основан на TOML. Есть две секции -- InterfaceToml и PeerToml. Секция PeerToml может повторятся много раз, но может быть только один InterfaceToml.

* InterfaceToml может содержать следующие поля:

    * PrivateKey — base64 приватный ключ сгенерированный wg genkey. Обязательный.
    
    * Address — список IP (v4 или v6) адресов (опционасльно с CIDR масками) которые будут назначенны интерфейсу. Необязталеьный; особое значени `auto` для получения адреса от сервера.

    * ListenPort — 16-bit порт для прослушивания. Необязательный; если не назначен выбирается случайным образом.

    * FwMark — 32-bit fwmark для исходящих пакетов. Если установлено 0 или "off", то эта функция выключена. Может быть задан в шеснадцатиричном формате "0x". Необязательный.

* PeerToml секции могут содержать следующие поля:

    * PublicKey — a base64 public key calculated by wg pubkey from a private key, and usually transmitted out of band to the author of the configuration file. Required.

    * PresharedKey — a base64 preshared key generated by wg genpsk. Optional, and may be omitted. This option adds an additional layer of symmetric-key cryptography to be mixed into the already existing public-key cryptography, for post-quantum resistance.

    * AllowedIPs — a comma-separated list of IP (v4 or v6) addresses with CIDR masks from which incoming traffic for this Peers is allowed and to which outgoing traffic for this Peers is directed. The catch-all 0.0.0.0/0 may be specified for matching all IPv4 addresses, and ::/0 may be specified for matching all IPv6 addresses. May be specified multiple times.

    * Endpoint — an endpoint IP or hostname, followed by a colon, and then a port number. This endpoint will be updated automatically to the most recent source IP address and port of correctly authenticated packets from the Peers. Optional.

    * PersistentKeepalive — a seconds interval, between 1 and 65535 inclusive, of how often to send an authenticated empty packet to the Peers for the purpose of keeping a stateful firewall or NAT mapping valid persistently. For example, if the interface very rarely sends traffic, but it might at anytime receive traffic from a Peers, and it is behind NAT, the interface might benefit from having a persistent keepalive interval of 25 seconds. If set to 0 or "off", this option is disabled. By default or when unspecified, this option is off. Most users will not need this. Optional.

