package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/kenit/wg-dynamic/config"
	old "gitlab.com/kenit/wg-dynamic/quick"
)

func init() {
	rootCmd.AddCommand(quickCmd)
}

var quickCmd = &cobra.Command{
	Use:   "quick",
	Short: "wg-dynamic quick [ up | down ] INTERFACE",
	Long: `wg-dynamic quick [ up | down ] INTERFACE
wg-quick compatibility interface. INTERFACE is an interface name,
with configuration found at ‘/etc/wg-dynamic/INTERFACE.yaml’
scripts and DNS not working`,
	Example: "wg-dynamic quick up wg0",
	Version: "v0.0.2",
	Args:    cobra.ExactArgs(2),
	Run:     quick,
}

func quick(cmd *cobra.Command, args []string) {
	// чтение конфига
	viper.SetConfigName(args[1])
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	yaml := config.ConfigYaml{}
	err = viper.Unmarshal(&yaml)
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// перевод конфига в читаемый формат
	cfg, err := config.Casting(yaml)
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	switch args[0] {
	case "up":
		old.QuickUp(args[1], *cfg)
	case "down":
		old.QuickDown(args[1], *cfg)
	default:
		cmd.Help()
		return
	}

}
