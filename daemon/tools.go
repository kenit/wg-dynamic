package daemon

import (
	"context"
	"errors"
	"gitlab.com/kenit/wg-dynamic/config"
	log "gitlab.com/kenit/wg-dynamic/log"
	pb "gitlab.com/kenit/wg-dynamic/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Tools struct {
	daemon  *Daemon
	log     log.Logger
	service *rpcTools
	stop    func()
}

//CreateTools create new Tools ready for work
func (d *Daemon) CreateTools(log log.Logger) *Tools {
	var service rpcTools
	tool := Tools{
		log:     log,
		service: &service,
		stop:    d.rpc.GracefulStop,
		daemon:  d,
	}
	service.Tools = &tool
	pb.RegisterDaemonServer(d.rpc, tool.service)
	d.log.Debug("tools was created")
	return &tool
}

func (t *Tools) version() (Major uint32, Minor uint32, Patch uint32) {
	t.log.Debug("call version")
	return MAJOR, MINOR, PATCH
}

func (t *Tools) Close() uint32 {
	t.log.Debug("call exit")
	go t.stop()
	return 0
}

func (t *Tools) verifyConfigFromRaw(cfg string) error {
	t.log.Debug("verify config from raw")
	conf, err := config.ParseRawConfig([]byte(cfg))
	if err != nil {
		return err
	}
	return conf.Validate()
}

func (t *Tools) verifyConfigFromFile(filePath string) error {
	t.log.Debug("verify config from file")
	conf, err := config.ParseFileConfig(filePath)
	if err != nil {
		return err
	}
	return conf.Validate()
}

func (t *Tools) createDeviceFromRaw(name string, cfg string) error {
	conf, err := config.ParseRawConfig([]byte(cfg))
	if err != nil {
		return err
	}
	err = conf.Validate()
	if err != nil {
		return err
	}
	err = t.daemon.CreateDevice(t.log, name, conf, "")

	if err != nil {
		return err
	}
	return nil
}

func (t *Tools) createDeviceFromFile(name string, filePath string) error {
	conf, err := config.ParseFileConfig(filePath)
	if err != nil {
		return err
	}
	err = conf.Validate()
	if err != nil {
		return err
	}
	err = t.daemon.CreateDevice(t.log, name, conf, filePath)
	if err != nil {
		return err
	}
	return nil
}

func (t *Tools) deviceUp(name string) error {
	dev, ok := t.daemon.devices[name]
	if !ok {
		return errors.New("not exist in daemon")
	}
	return dev.link.Up()
}

func (t *Tools) deviceDelete(name string) error {
	dev, ok := t.daemon.devices[name]
	if !ok {
		return errors.New("not exist in daemon")
	}
	return dev.link.Delete()
}

type rpcTools struct {
	pb.UnimplementedDaemonServer
	*Tools
}

func (r *rpcTools) VerifyConfig(ctx context.Context, request *pb.VerifyConfigRequest) (*pb.VerifyConfigReply, error) {
	var replay pb.VerifyConfigReply
	switch x := request.Config.(type) {
	case *pb.VerifyConfigRequest_Path:
		r.log.Debugf("get path: %s", x.Path)
		err := r.verifyConfigFromFile(x.Path)
		if err != nil {
			replay.Valid = false

		} else {
			replay.Valid = true
		}
		return &replay, nil

	case *pb.VerifyConfigRequest_File:
		r.log.Debugf("get file: %s", x.File)
		err := r.verifyConfigFromRaw(x.File)
		if err != nil {
			replay.Valid = false

		} else {
			replay.Valid = true
		}
		return &replay, nil
	default:
		return nil, errors.New("internal server error")
	}
}

func (r *rpcTools) Version(ctx context.Context, empty *emptypb.Empty) (*pb.VersionReplay, error) {
	Major, Minor, Patch := r.version()
	return &pb.VersionReplay{
		Major: Major,
		Minor: Minor,
		Patch: Patch,
	}, nil
}

func (r *rpcTools) Exit(ctx context.Context, empty *emptypb.Empty) (*pb.ExitReplay, error) {
	code := r.Close()
	return &pb.ExitReplay{Code: code}, nil
}

func (r *rpcTools) Create(ctx context.Context, request *pb.CreateRequest) (*pb.CreateReply, error) {
	var replay pb.CreateReply
	switch x := request.Config.(type) {
	case *pb.CreateRequest_Path:
		err := r.createDeviceFromFile(request.Name, x.Path)
		if err != nil {
			replay.Valid = false

		} else {
			replay.Valid = true
		}
		return &replay, nil

	case *pb.CreateRequest_File:
		err := r.createDeviceFromRaw(request.Name, x.File)
		if err != nil {
			replay.Valid = false

		} else {
			replay.Valid = true
		}
		return &replay, nil
	default:
		return nil, errors.New("internal server error")
	}
}

func (r *rpcTools) Up(ctx context.Context, request *pb.UpRequest) (*pb.UpReply, error) {
	var replay pb.UpReply
	err := r.deviceUp(request.Name)
	if err != nil {
		replay.Valid = false
	} else {
		replay.Valid = true
	}
	return &replay, nil
}

//func (r rpcTools) Down(ctx context.Context, request *pb.DownRequest) (*pb.DownReply, error) {
//	panic("implement me")
//}

func (r *rpcTools) Delete(ctx context.Context, request *pb.DeleteRequest) (*pb.DeleteReply, error) {
	var replay pb.DeleteReply
	err := r.deviceUp(request.Name)
	if err != nil {
		replay.Valid = false
	} else {
		replay.Valid = true
	}
	return &replay, nil
}
