package daemon

import (
	log "gitlab.com/kenit/wg-dynamic/log"
	"google.golang.org/grpc"
	"net"
)

const (
	MAJOR = 0
	MINOR = 1
	PATCH = 0
)

type Daemon struct {
	log               log.Logger
	tools             *Tools
	rpc               *grpc.Server
	devices           map[string]Device
}

func NewDaemon(log log.Logger) Daemon {
	return Daemon{
		log:               log,
		rpc:               grpc.NewServer(),
		devices:           make(map[string]Device),
	}
}

//Start begin non blocking daemon
func (d Daemon) Start(listener net.Listener) {
	go d.Serve(listener)
}

//Serve begin blocking daemon
func (d Daemon) Serve(listener net.Listener) error {
	d.log.Info("tools started")
	return d.rpc.Serve(listener)
}

//Close graceful stop stop daemon
func (d Daemon) Close() {
	d.tools.Close()
}
