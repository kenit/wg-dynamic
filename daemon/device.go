package daemon

import (
	cfg "gitlab.com/kenit/wg-dynamic/config"
	log "gitlab.com/kenit/wg-dynamic/log"
	"gitlab.com/kenit/wg-dynamic/netlink"
)

type Device struct {
	log        log.Logger
	name       string
	configPath string
	config     *cfg.Config
	link       netlink.Link
}

func (d *Daemon) CreateDevice(log log.Logger, name string, config *cfg.Config, path string) error {
	var device Device
	link := netlink.NewLink(name)
	err := link.Create()
	if err != nil {
		return  err
	}

	err = link.Configure(config)
	if err != nil {
		return  err
	}
	// create device struct
	device.name = name
	device.link = link
	device.config = config
	device.configPath = path
	device.log = log

	d.devices[name] = device

	d.log.Debugf("device %s was created", name)
	return  nil
}
