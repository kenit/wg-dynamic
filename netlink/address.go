package netlink

import "github.com/vishvananda/netlink"

type AddressService interface {
	AddrAdd(addr string) error
	AddrDel(addr string) error
}

type addrService struct {
	link netlink.Link
}

func NewAddressService(name string) (AddressService, error) {
	link, err := netlink.LinkByName(name)
	if err != nil {
		return nil, err
	}
	serv := addrService{link: link}
	return serv, nil
}

func (a addrService) AddrAdd(addr string) error {
	address, err := netlink.ParseAddr(addr)
	if err != nil {
		return err
	}
	return netlink.AddrAdd(a.link, address)
}

func (a addrService) AddrDel(addr string) error {
	address, err := netlink.ParseAddr(addr)
	if err != nil {
		return err
	}
	return netlink.AddrDel(a.link, address)
}


