package netlink

import (
	"github.com/vishvananda/netlink"
)

const (
	TABLE_MAIN = 254
)

type RouteService interface {
	RouteAdd(route string) error
	RouteDel(route string) error
}

type RouterOptions func(RouteService)

//WithTable set table
func WithTable(table int) RouterOptions {
	return func(r RouteService) {
		var serv, _ = r.(*routeService)
		serv.table = table
	}
}

type routeService struct {
	name  string
	table int
}

func NewRouterService(name string, options ...RouterOptions) (RouteService, error) {
	var service routeService
	service.name = name
	service.table = TABLE_MAIN

	for _, optionFn := range options {
		optionFn(&service)
	}
	return service, nil
}

func (s routeService) RouteAdd(route string) error {

	rou, err := netlink.ParseAddr(route)
	if err != nil {
		return err
	}
	link, err := netlink.LinkByName(s.name)
	if err != nil {
		return err
	}
	rawRoute := netlink.Route{
		LinkIndex: link.Attrs().Index,
		Scope:     netlink.SCOPE_LINK,
		Dst:       rou.IPNet,
		Table:     s.table,
	}

	err = netlink.RouteAdd(&rawRoute)
	if err != nil {
		return err
	}
	return nil
}

func (s routeService) RouteDel(route string) error {

	rou, err := netlink.ParseAddr(route)
	if err != nil {
		return err
	}
	link, err := netlink.LinkByName(s.name)
	if err != nil {
		return err
	}
	rawRoute := netlink.Route{
		LinkIndex: link.Attrs().Index,
		Scope:     netlink.SCOPE_LINK,
		Dst:       rou.IPNet,
		Table:     s.table,
	}

	err = netlink.RouteDel(&rawRoute)
	if err != nil {
		return err
	}
	return nil
}
