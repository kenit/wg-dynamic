package netlink

import (
	"gitlab.com/kenit/wg-dynamic/config"
	"os/exec"
	"strings"
)

type Device struct {
	name     string
	LinkSrv  Link
	AddrSrv  AddressService
	WGSrv    WireguardService
	RouteSrv RouteService
	config   *config.Config
}

func NewDevice(name string, config *config.Config) (*Device, error) {
	link := NewLink(name)
	addrSrv, err := NewAddressService(name)
	if err != nil {
		return nil, err
	}
	wgSrv, err := NewWireguardService(name)
	if err != nil {
		return nil, err
	}
	routeSrv, err := NewRouterService(name)
	if err != nil {
		return nil, err
	}
	return &Device{
		name:     name,
		LinkSrv:  link,
		AddrSrv:  addrSrv,
		WGSrv:    wgSrv,
		RouteSrv: routeSrv,
		config:   config,
	}, nil

}

func (d *Device) Close() error {
	return d.WGSrv.Close()
}

func (d Device) Create() error {

	err := d.LinkSrv.Create()
	if err != nil {
		return err
	}

	err = d.WGSrv.Configure(d.config)
	if err != nil {
		d.LinkSrv.Delete()
		return err
	}

	for _, addr := range d.config.Interface.Address {
		err = d.AddrSrv.AddrAdd(addr)
		if err != nil {
			d.LinkSrv.Delete()
			return err
		}
	}
	return nil
}

func (d Device) Up() error {
	err := hookRun(d.name, d.config.Interface.PreUp)
	if err != nil {
		//show error
	}

	err = d.LinkSrv.Up()
	if err != nil {
		return err
	}

	//todo proper handle route add error
	for _, peer := range d.config.Peer {
		for _, route := range peer.AllowedIPs {
			err = d.RouteSrv.RouteAdd(route)
			if err != nil {
				return err
			}
		}
	}
	err = hookRun(d.name, d.config.Interface.PostUp)
	if err != nil {
		//show error
	}
	return nil
}

func (d Device) Down() error {
	err := hookRun(d.name, d.config.Interface.PreDown)
	if err != nil {
		//show error
	}

	err = d.LinkSrv.Down()
	if err != nil {
		return err
	}

	//todo proper handle route del error
	for _, peer := range d.config.Peer {
		for _, route := range peer.AllowedIPs {
			err = d.RouteSrv.RouteDel(route)
			if err != nil {
				return err
			}
		}
	}
	err = hookRun(d.name, d.config.Interface.PostDown)
	if err != nil {
		//show error
	}
	return nil
}

func (d Device) Delete() error {

	err := d.LinkSrv.Delete()
	if err != nil {
		return err
	}

	return nil
}

//hookRun start users scripts
func hookRun(name, script string) error {
	if script != "" {
		command := strings.ReplaceAll(script, "%i", name)
		err := exec.Command("/bin/sh", "-c", command).Run()
		if err != nil {
			return err
		}
	}
	return nil
}
