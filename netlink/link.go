package netlink

// todo сделать нормальную обработку ошибок а не просто проброс наверх
// чтоб они на месте фиксили и это было без последствий для системы

// todo добавить sqlite как базу для знания точного воздействия программы на систему по маршрутам, адресам и правилам
// для упрощения манипуляции данными сущностями

// todo добавить тесты и комментарии для документации

// todo добавить логер (?) возможно он требует переработки

// todo добавить состояние интерфейса для избежания возможного выстрела в колено по типу запуска не сконфигурированного интерфейса
import (
	"errors"
	"github.com/vishvananda/netlink"
	"net"
)

//const (
//	MTU = 1420
//)

type Link interface {
	Create() error
	Up() error
	Down() error
	Delete() error
}

type LinkOptions func(Link)

type link struct {
	attrs    netlink.LinkAttrs
}

func (l link) Create() error {
	err := checkExistInterface(l.attrs.Name)
	if err != nil {
		return err
	}
	return netlink.LinkAdd(l)
}

func (l link) Up() error {
	// запуск прехука
	//err := hookRun(l.attrs.Name, l.PreUp)
	//if err != nil {
	//	return err
	//}
	// подъём интерфейса
	err := netlink.LinkSetUp(l)
	if err != nil {
		return err
	}

	// навешивание адресов на интерфейс
	//for _, addr := range l.cfg.Interface.Address {
	//	err = l.AddressAdd(addr)
	//	if err != nil {
	//		return err
	//	}
	//}

	// сбор всех доступных адресов
	//var routes []string
	//for _, peer := range l.cfg.Peer {
	//	routes = append(routes, peer.AllowedIPs...)
	//}

	// добавление всех адресов в таблицу маршрутизации
	//for _, route := range routes {
	//	err := l.RouteAdd(route)
	//	if err != nil {
	//		return err
	//	}
	//}

	// запуск пост хука
	//err = hookRun(l.attrs.Name, l.PostUp)
	//if err != nil {
	//	return err
	//}
	return nil
}

func (l link) Down() error {

	err := netlink.LinkSetDown(l)
	if err != nil {
		return err
	}

	return nil
}

func (l link) Delete() error {
	err := netlink.LinkDel(l)
	if err != nil {
		return err
	}
	return nil
}

func NewLink(name string, options ...LinkOptions) Link {

	linkAttrs := netlink.NewLinkAttrs()
	linkAttrs.Name = name

	var device link
	device.attrs = linkAttrs
	for _, optionFn := range options {
		optionFn(&device)
	}
	return &device
}

func (l link) Attrs() *netlink.LinkAttrs {
	return &l.attrs
}

func (l link) Type() string {
	return "wireguard"
}

//checkExistInterface for check exist or not interface
func checkExistInterface(name string) error {

	//check from all devices
	_, err := net.InterfaceByName(name)
	if err == nil {
		return errors.New("interface " + name + " already exist")
	}

	//not found
	return nil
}

