package netlink

import (
	"gitlab.com/kenit/wg-dynamic/config"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"net"
	"time"
)

type WireguardService interface {
	Configure(config *config.Config) error
	Close() error
}

type wgSetvice struct {
	name   string
	client *wgctrl.Client
}

func NewWireguardService(name string) (WireguardService, error) {
	client, err := wgctrl.New()
	if err != nil {
		return nil, err
	}

	return &wgSetvice{
		name:   name,
		client: client,
	}, nil
}

func (s wgSetvice) Close() error {
	err := s.client.Close()
	return err
}

func (s wgSetvice) Configure(config *config.Config) error {

	// преобразование конфига интерфейса
	wgConfig, err := parseConfig(*config)

	err = s.client.ConfigureDevice(s.name, wgConfig)
	if err != nil {
		return err
	}
	return nil
}

// parseConfig get full device config
func parseConfig(config config.Config) (wgtypes.Config, error) {
	var deviceConfig wgtypes.Config

	// преобразование конфига интерфейса
	privateKey, err := wgtypes.ParseKey(config.Interface.PrivateKey)
	if err != nil {
		return deviceConfig, err
	}
	deviceConfig = wgtypes.Config{
		PrivateKey:   &privateKey,
		ReplacePeers: false,
	}

	if config.Interface.ListenPort != 0 {
		deviceConfig.ListenPort = &config.Interface.ListenPort
	}

	if config.Interface.FwMark != 0 {
		deviceConfig.FirewallMark = &config.Interface.FwMark
	}

	peerConfig, err := parseConfigPeer(config)
	if err != nil {
		return deviceConfig, err
	}
	deviceConfig.Peers = peerConfig

	return deviceConfig, nil

}

// parseConfigPeer get peerConfig slice
func parseConfigPeer(config config.Config) ([]wgtypes.PeerConfig, error) {
	var peerConfig []wgtypes.PeerConfig

	// преобразование конфига пира
	for _, peer := range config.Peer {

		//публичный ключ (всегда есть)
		publicKey, err := wgtypes.ParseKey(peer.PublicKey)
		if err != nil {
			return nil, err
		}
		wgPeerCfg := wgtypes.PeerConfig{
			PublicKey:         publicKey,
			Remove:            false,
			UpdateOnly:        false,
			ReplaceAllowedIPs: false,
		}

		if peer.Endpoint != "" {
			endpoint, _ := net.ResolveUDPAddr("udp", peer.Endpoint)
			wgPeerCfg.Endpoint = endpoint
		}

		if peer.PresharedKey != "" {
			presharedKey, _ := wgtypes.ParseKey(peer.PresharedKey)
			wgPeerCfg.PresharedKey = &presharedKey
		}

		if peer.PersistentKeepalive != 0 {
			persistentKeepalive := time.Duration(peer.PersistentKeepalive)
			wgPeerCfg.PersistentKeepaliveInterval = &persistentKeepalive
		}

		if peer.AllowedIPs != nil {
			for _, allow := range peer.AllowedIPs {
				_, ipnet, _ := net.ParseCIDR(allow)
				wgPeerCfg.AllowedIPs = append(wgPeerCfg.AllowedIPs, *ipnet)
			}
		}

		//сборка всех пиров в слайс
		peerConfig = append(peerConfig, wgPeerCfg)

	}

	return peerConfig, nil
}
