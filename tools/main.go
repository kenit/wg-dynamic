package main

import (
	"context"
	pb "gitlab.com/kenit/wg-dynamic/proto"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"net"
	"runtime"
	"time"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	"os"
)

const (
	SOCK = "wg-dynamic.sock"
)

var log zap.SugaredLogger

func init() {
	logger, _ := zap.NewProduction()
	log = *logger.Sugar()
}

func main() {
	var sock string
	switch runtime.GOOS {
	case "linux":
		sock = os.TempDir() + "/" + SOCK
	case "windows":
		sock = os.TempDir() + "\\" + SOCK
	default:
		log.Panic("can't get OS")
	}
	conn, err := grpc.Dial(sock, grpc.WithInsecure(), grpc.WithBlock(), grpc.WithDialer(func(addr string, timeout time.Duration) (net.Conn, error) {
		return net.DialTimeout("unix", addr, timeout)
	}))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewDaemonClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Version(ctx, &emptypb.Empty{})
	if err != nil {
		log.Fatalf("could not get version: %v", err)
	}
	log.Infof("Version: v%d.%d.%d", r.Major, r.Minor, r.Patch)

}
