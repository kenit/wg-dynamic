package main

import (
	"errors"
	"gitlab.com/kenit/wg-dynamic/daemon"
	"gitlab.com/kenit/wg-dynamic/log"
	"net"
	"os"
	"runtime"
)

const (
	SOCK = "wg-dynamic.sock" //	sock = os.TempDir() + "/" + SOCK
)

//go:generate protoc proto/daemon.proto --go-grpc_out=. --go_out=.
func main() {
	logger := log.GetLogger()
	logger.Info("logger init success")

	if err := permissionCheck(); err != nil {
		logger.Fatalf("permission error: %s", err)
	}
	serv := daemon.NewDaemon(logger)

	serv.CreateTools(log.GetNamedLogger("tools"))

	listener, _ := net.Listen("tcp", ":9000")
	serv.Serve(listener)
	logger.Debug("work done")
}

func permissionCheck() error {
	switch runtime.GOOS {
	case "linux":
		if os.Geteuid() != 0 {
			return errors.New("wg-dynamic must be run as root")
		}
		return nil
	case "windows":
		return nil
	default:
		return errors.New("can't detect os")
	}
}
