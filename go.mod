module gitlab.com/kenit/wg-dynamic

go 1.16

require (
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/vishvananda/netlink v1.1.0
	go.uber.org/zap v1.16.0
	golang.zx2c4.com/wireguard/wgctrl v0.0.0-20200609130330-bd2cb7843e1b
	google.golang.org/grpc v1.35.0
	google.golang.org/protobuf v1.25.0
)
