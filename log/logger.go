package log

import (
	"go.uber.org/zap"
	"os"
)

//Logger interface for logger agnostic implement
type Logger interface {
	Debug(args ...interface{})
	Debugf(template string, args ...interface{})

	Error(args ...interface{})
	Errorf(template string, args ...interface{})

	Fatal(args ...interface{})
	Fatalf(template string, args ...interface{})

	Info(args ...interface{})
	Infof(template string, args ...interface{})

	Panic(args ...interface{})
	Panicf(template string, args ...interface{})

	Warn(args ...interface{})
	Warnf(template string, args ...interface{})
}

var logger *zap.SugaredLogger

func init() {
	config := zap.NewDevelopmentConfig()
	log, err := config.Build()
	if err != nil {
		os.Exit(1)
	}
	logger = log.Sugar()
}

func GetLogger() Logger {
	return logger
}

func GetDummyLogger() Logger {
	return dummyLogger{}
}

func GetNamedLogger(name string) Logger {
	return logger.Named(name)
}

func Debug(args ...interface{}) {
	logger.Debug(args)
}

func Debugf(template string, args ...interface{}) {
	logger.Debugf(template, args)
}

func Error(args ...interface{}) {
	logger.Error(args)
}

func Errorf(template string, args ...interface{}) {
	logger.Errorf(template, args)
}

func Fatal(args ...interface{}) {
	logger.Fatal(args)
}

func Fatalf(template string, args ...interface{}) {
	logger.Fatalf(template, args)
}

func Info(args ...interface{}) {
	logger.Info(args)
}

func Infof(template string, args ...interface{}) {
	logger.Infof(template, args)
}

func Panic(args ...interface{}) {
	logger.Panic(args)
}

func Panicf(template string, args ...interface{}) {
	logger.Panicf(template, args)
}

func Warn(args ...interface{}) {
	logger.Warn(args)
}

func Warnf(template string, args ...interface{}) {
	logger.Warnf(template, args)
}
